﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            foreach (var item in Itens)
            {
                if (!item.Nome.Equals("Dente do Tarrasque"))
                    item.PrazoParaVenda -= 1;

                switch (item.Nome)
                {
                    case "Queijo Brie Envelhecido":
                        Incrementar(item);
                        if (item.PrazoParaVenda < 0)
                            Incrementar(item);
                        break;
                    case "Ingressos para o concerto do Turisas":
                        Incrementar(item);

                        if (item.PrazoParaVenda < 10)
                            Incrementar(item);
                        if (item.PrazoParaVenda < 5)
                            Incrementar(item);
                        if (item.PrazoParaVenda < 0)
                            item.Qualidade = 0;
                        break;
                    case "Bolo de Mana Conjurado":
                        Decrementar(item, 2);
                        break;
                    case "Dente do Tarrasque":
                        break;
                    default:
                        Decrementar(item, 1);
                        if (item.PrazoParaVenda < 0)
                            Decrementar(item, 1);
                        break;
                }
            }
        }

        private static void Decrementar(Item item, int quantidade)
        {
            if (item.Qualidade > 0)
                item.Qualidade -= quantidade;
        }

        private static void Incrementar(Item item)
        {
            if (item.Qualidade < 50)
                item.Qualidade += 1;
        }
    }
}
