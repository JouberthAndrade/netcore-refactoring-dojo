﻿using ApprovalTests.Combinations;
using ApprovalTests.Reporters;
using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    [UseReporter(typeof(DiffReporter))]
    public class GildedRoseTest
    {
        [Fact]
        public void foo()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "foo", PrazoParaVenda = 0, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("foo", Items[0].Nome);
        }

        [Fact]
        public void combinationApprovalTest_AtualizarQualidade()
        {
            CombinationApprovals.VerifyAllCombinations(AtualizarQualidade,
                aList: new string[] { "Queijo Brie Envelhecido", "Ingressos para o concerto do Turisas", "Bolo de Mana Conjurado", "Dente do Tarrasque" },
                bList: new int[] { -1, 0, 11 },
                cList: new int[] { 0, 1, 49, 50 });

        }

        private string AtualizarQualidade(string nome, int prazo, int qualidade)
        {
            IList<Item> Items = new List<Item> { new Item { Nome = nome, PrazoParaVenda = prazo, Qualidade = qualidade } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            return string.Format("{0}-{1}", Items[0].Nome, Items[0].Qualidade);
        }
    }
}
